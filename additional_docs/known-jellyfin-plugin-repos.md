# Known Jellyfin Plugin Repositories
A List of all known jellyfin plugin Repositories.
- Credit: [u/Navarian_](https://www.reddit.com/user/Navarian_/) on [Reddit](https://reddit.com/)
- Note: Duplicates across repos exist, some repos provide later versions etc
- WARNING: I didn't make these Repositories or their content, Any 3rd party plugin may not fully work properly or may contain malicious code.

#### Official Jellyfin Stable:
```
https://repo.jellyfin.org/releases/plugin/manifest-stable.json
```

#### Crobibero 
```
https://repo.codyrobibero.dev/manifest.json
```

#### Crobibero2
```
https://raw.githubusercontent.com/crobibero/Jellyfin.Channels.LazyMan/master/manifest.json
```

#### Danieladov
```
https://raw.githubusercontent.com/danieladov/JellyfinPluginManifest/master/manifest.json
```

#### Simoni
```
https://simoni.dev/jellyfin/repo.json
```

#### Imaginaryupside
```
https://raw.githubusercontent.com/imaginary-upside/JellyfinJav/master/manifest.json
```

#### Shemanaev
```
https://raw.githubusercontent.com/shemanaev/jellyfin-plugin-repo/master/manifest.json
```

### Oddstr13 (unstable)
```
https://mirror3.openshell.no/jellyfin/plugins/unstable.json
```

#### MrTimscampi
```
https://raw.githubusercontent.com/MrTimscampi/jellyfin-repo/master/manifest.json
```

#### Jesseward
```
https://jellyfin-repo.jesseward.com/manifest.json
```

#### ShokoAnime
```
https://raw.githubusercontent.com/ShokoAnime/Shokofin/master/manifest.json
```

#### ShokoAnime (unstable)
```
https://raw.githubusercontent.com/ShokoAnime/Shokofin/master/manifest-unstable.json
```

#### Lyarenei
```
https://raw.githubusercontent.com/lyarenei/jellyfin-plugin-listenbrainz/master/manifest.json
```

#### Linfor
```
https://raw.githubusercontent.com/LinFor/jellyfin-plugin-kinopoisk/master/dist/manifest.json
```

#### K-matti
```
https://raw.githubusercontent.com/k-matti/jellyfin-plugin-repository/master/manifest.json
```

#### N0tflix
```
https://raw.githubusercontent.com/n0tMaster/n0tFlix-Main/main/Manifest.json
```


#### PhoenixAdult
```
https://raw.githubusercontent.com/DirtyRacer1337/Jellyfin.Plugin.PhoenixAdult/master/manifest.json
```

#### AnilistSync
```
https://raw.githubusercontent.com/ARufenach/AnilistSync/master/manifest.json
```

#### ThePornDatabase
```
https://raw.githubusercontent.com/ThePornDatabase/Jellyfin.Plugin.ThePornDB/main/manifest.json
```

#### ankenyr
```
https://raw.githubusercontent.com/ankenyr/jellyfin-plugin-repo/master/manifest.json
```
