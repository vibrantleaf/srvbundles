#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public\
  --new-service=container_jfa-go \
  --add-port=8056/tcp
sudo firewall-cmd --reload
