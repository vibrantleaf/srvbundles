#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public \
  --new-service=container_mumble \
  --add-port=64738/tcp \
  --add-port=64738/udp
sudo firewall-cmd --reload