#!/usr/bin/env bash
# tdarr port(s)
sudo firewall-cmd \
  --permanent \
  --zone=public \
  --new-service=docker_tdarr \
  --add-port=8265/tcp \
  --add-port=8266/tcp \
  --add-port=8268/tcp
sudo firewall-cmd --reload
