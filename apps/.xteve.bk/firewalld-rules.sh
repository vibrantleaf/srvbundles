#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public \
  --new-service=container_xteve \
  --add-port=34400/tcp
sudo firewall-cmd --reload