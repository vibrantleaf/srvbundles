#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public \
  --new-service=container_unmanic \
  --add-port=UNMANIC-PORT/tcp
sudo firewall-cmd --reload
