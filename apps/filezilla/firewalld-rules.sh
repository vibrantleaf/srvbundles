#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public \
  --new-service=container_filezilla \
  --add-port=FILEZILLA-PORT/tcp
sudo firewall-cmd --reload
