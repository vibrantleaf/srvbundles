#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public \
  --new-service=container_sonarr \
  --add-port=8989/tcp
sudo firewall-cmd --reload
