#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public \
  --new-service=container_jellyseerr \
  --add-port=5055/tcp
sudo firewall-cmd --reload