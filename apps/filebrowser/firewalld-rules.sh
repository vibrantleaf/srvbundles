#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public \
  --new-service=container_filebrowser \
  --add-port=FILEBROWSER-PORT/tcp
sudo firewall-cmd --reload