#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public \
  --new-service=container_jellyfin \
  --add-port=8096/tcp \
  --add-port=8920/tcp \
  --add-port=1900/udp \
  --add-port=7359/udp
sudo firewall-cmd --reload