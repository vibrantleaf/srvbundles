#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public \
  --new-service=container-emby \
  --add-port=EMBY-PORT/tcp
sudo firewall-cmd --reload