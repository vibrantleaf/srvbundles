#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public\
  --new-service=docker_syncthing \
  --add-port=8384/tcp \
  --add-port=22000/tcp \
  --add-port=22000/udp \
  --add-port=21027/udp
sudo firewall-cmd --reload