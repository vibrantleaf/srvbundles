#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public \
  --new-service=container_lidarr \
  --add-port=8686/tcp
sudo firewall-cmd --reload
