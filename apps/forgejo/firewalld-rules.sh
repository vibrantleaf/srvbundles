#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public \
  --new-service=container_forgejo \
  --add-port=FORGEJO-HTTP-PORT/tcp \
  --add-port=FORGEJO-SSH-PORT/tcp
sudo firewall-cmd --reload