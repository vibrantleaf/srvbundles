#!/usr/bin/env bash
set -x
string_a=$(openssl rand -base64 32 | tr -dc 'a-zA-Z0-9' | head -c 36 )
string_b=$(openssl rand -base64 32 | tr -dc 'a-zA-Z0-9' | head -c 36 )
string_c=$(openssl rand -base64 32 | tr -dc 'a-zA-Z0-9' | head -c 36 )
string_d=$(openssl rand -base64 32 | tr -dc 'a-zA-Z0-9' | head -c 36 )

sed -i 's/changeme_a/$string_a/g' ./environment.forgejo.cfg
sed -i 's/changeme_b/$string_b/g' ./environment.forgejo.cfg
sed -i 's/changeme_c/$string_c/g' ./environment.forgejo.cfg
sed -i 's/changeme_d/$string_d/g' ./environment.forgejo.cfg