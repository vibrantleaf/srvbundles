#!/usr/bin/env bash
set -xout pipefail
#
# Licenced as CC-BY-NC-3.0-AU by vibrantleaf
#
function dependency_check {
  if [ -f $(which magick) ]
  then
   continue
  else
    echo 'ERROR: ImageMagick not found'
    exit 1
  fi
  if [ -f $(which ffmpeg) ]
  then
    continue
  else
    echo 'ERROR: FFmpeg not found'
    exit 1
  fi
}
function main {
  if [ -z ${DIRECTORY+x} ]
  then
    echo 'DIRECTORY variable is unset assuming DIRECTORY=$(pwd)'
    echo 'use env DIRECTORY=/path/to/files convert_to_opus.sh to set the directory that this script will run on'
    DIRECTORY=$(pwd)
  fi
  if [ -z ${MUSIC+x} ]
  then
    echo 'MUSIC variable is unset assuming MUSIC=yes'
    echo 'use env MUSIC=no convert_to_opus.sh for spoken word'
    MUSIC=yes
  fi
  for FILE in "$DIRECTORY"/*
  do
    if [[ "$FILE" == *.opus ]]
    then
      continue
    elif [[ "$FILE" == *.png ]]
    then
      magick convert "$FILE" cover.jpg
    elif [[ "$FILE" == *.webp ]]
    then
      magick convert "$FILE" cover.jpg
    elif [[ "$FILE" == *.avif ]]
    then
      magick convert "$FILE" cover.jpg
    elif [[ "$FILE" == *.jpg ]]
     then
      if [[ "$FILE == cover.jpg" ]]
      then
        continue
      else
        mv "$FILE" cover.jpg
      fi
    elif [[ "$FILE" == *.jpeg ]]
    then
      mv "$FILE" cover.jpg
    elif [ -f "$FILE" ]
    then
      FILENAME=$(basename "$FILE" | sed 's/\(.*\)\..*/\1/')
      FILEDIR=$(dirname "$FILE")
      ffmpeg -y -i "$FILE" -f ffmetadata "$FILEDIR/$FILENAME.metadata.txt"
      if [ "$MUSIC" -eq yes ]
      then        
        ffmpeg -y -i "$FILE" -c:a libopus -b:a 192k -map_metadata 0 -map_chapters 0 -id3v2_version 3 "$FILEDIR/$FILENAME.opus"
      else
        ffmpeg -y -i "$FILE" -c:a libopus -b:a 64k -map_metadata 0 -map_chapters 0 -id3v2_version 3 "$FILEDIR/$FILENAME.opus"
      fi
      if [ $? -eq 0 ]; then
        rm "$FILE"
      else
        printf "Conversion failed for $FILE. Original file not removed.\n"
      fi
    fi
  done
}
dependency_check
main